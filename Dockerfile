FROM amazoncorretto:21-alpine-jdk
WORKDIR /app
COPY . .
RUN ./mvnw clean package

FROM amazoncorretto:21-alpine
WORKDIR /app
COPY --from=0 /app/target/demo-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]
